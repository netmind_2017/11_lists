package com.example.gelso.snipper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    ListView lstView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
        layout();
    }

    public void layout() {

        //LOAD LIST VIEW
        lstView = (ListView) findViewById(R.id.lstView);

        List<String> list = new ArrayList<String>();
        list.add("list 1");
        list.add("list 2");
        list.add("list 3");
        list.add("list 4");
        list.add("list 5");
        list.add("list 6");
        list.add("list 7");
        list.add("list 8");
        list.add("list 9");
        list.add("list 10");
        list.add("list 11");
        list.add("list 12");
        list.add("list 13");
        list.add("list 14");
        list.add("list 15");
        list.add("list 16");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

        lstView.setAdapter(adapter);


        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "item selected :" + ((TextView) view).getText() +  " id: "  +  position, Toast.LENGTH_SHORT).show();
            }
        });

    }

}
